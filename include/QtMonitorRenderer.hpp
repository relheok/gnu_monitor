/*
 * =====================================================================================
 *
 *       Filename:  QtMonitorRenderer.hpp
 *
 *    Description:
 *
 *        Created:  21/01/2017 20:53:36
 *
 *         Author:  Quentin Bazin, <quent42340@gmail.com>
 *
 * =====================================================================================
 */
#ifndef QTMONITORRENDERER_HPP_
#define QTMONITORRENDERER_HPP_

#include <QMainWindow>
#include <QVBoxLayout>

#include "AMonitorRenderer.hpp"

class QtMonitorRenderer : public AMonitorRenderer, public QMainWindow {
	public:
		QtMonitorRenderer();

		void update() override;

		void setupModule(size_t hashCode, Qt::DockWidgetArea area, Qt::Orientation orientation) override;

		void keyPressEvent(QKeyEvent *event) override;
};

#endif // QTMONITORRENDERER_HPP_
