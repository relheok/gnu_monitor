//
// TermcapsRenderer.hpp for gkrellm in /home/koehle_j/rendu/Piscine_tek2/cpp_gkrellm
//
// Made by Jérémy Koehler
// Login   <koehle_j@epitech.net>
//
// Started on  Sat Jan 21 22:47:16 2017 Jérémy Koehler
// Last update Sun Jan 22 09:30:49 2017 Quentin Bazin
//

#ifndef TERMCAPSRENDERER_H_
# define TERMCAPSRENDERER_H_

#include <ncurses.h>
#include "AMonitorRenderer.hpp"

namespace Termcaps {
  class Renderer : public AMonitorRenderer {
  public:
    Renderer();
    virtual ~Renderer() override;

    virtual void update() override;

    // bool killRenderer() const;
  };
}

#endif // TERMCAPSRENDERER_H_
