//
// TermcapsNAMES.hpp for gkrellm in /home/koehle_j/rendu/Piscine_tek2/cpp_gkrellm
//
// Made by Jérémy Koehler
// Login   <koehle_j@epitech.net>
//
// Started on  Sat Jan 21 22:44:49 2017 Jérémy Koehler
// Last update Sun Jan 22 09:30:49 2017 Quentin Bazin
//

#ifndef TERMCAPSNAMES_H_
# define TERMCAPSNAMES_H_

#include <ncurses.h>
#include <string>
#include <vector>
#include <map>
#include "AMonitorDisplay.hpp"

namespace Termcaps {
  class Names : public AMonitorDisplay {
  public:
    Names(int x, int y);
    virtual ~Names();

    virtual void update() override;

  private:
    int m_x;
    int m_y;
    WINDOW *m_win;
  };
}

#endif // TERMCAPSNAMES_H_
