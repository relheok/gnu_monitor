#ifndef CPU_HPP_
# define CPU_HPP_

# include <algorithm>
# include <fstream>
# include <sstream>
# include <iostream>
# include "AMonitorModule.hpp"

class	CPU : public AMonitorModule
{
public:
  CPU();
  CPU(AMonitorModule const &other);
  CPU &operator=(AMonitorModule const &);
  virtual ~CPU();
  virtual void		FillData();

  std::string UsageCPU(int i);
  std::string haveField(std::string, std::map<std::string, std::string>&);
  std::array<double, 9> &getArray(int);

private:
  std::vector<std::array<double, 9>> m_old;
};

#endif /* !CPU_HPP */
