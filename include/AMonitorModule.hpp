#ifndef AMONITORMODULE_HPP_
#define AMONITORMODULE_HPP_

#include "IMonitorModule.hpp"

class AMonitorModule : public IMonitorModule {
	public:
		AMonitorModule();
		AMonitorModule(std::string const &name, std::string const &filename, unsigned int const);
		AMonitorModule(AMonitorModule const &);
		virtual ~AMonitorModule();

		AMonitorModule &operator=(AMonitorModule const &);

		virtual void FillData() = 0;

		const std::map<std::string, std::string> &operator[](size_t) const override;
		std::map<std::string, std::string> &operator[](size_t) override;

		virtual std::string getName() const override;
		virtual std::string getFile() const override;
		virtual unsigned int getMax() const override;
		virtual unsigned int getSizeHistory() const override;

		virtual std::vector<std::map<std::string, std::string>> getData() const override;

	protected:
		std::string const m_name;
		std::string const m_filename;
		unsigned int const m_max;

		std::vector<std::map<std::string, std::string>> m_data;
};

#endif // AMONITORMODULE_HPP_
