/*
 * =====================================================================================
 *
 *       Filename:  AMonitorRenderer.hpp
 *
 *    Description:
 *
 *        Created:  21/01/2017 20:08:54
 *
 *         Author:  Quentin Bazin, <quent42340@gmail.com>
 *
 * =====================================================================================
 */
#ifndef AMONITORRENDERER_HPP_
#define AMONITORRENDERER_HPP_

#include <map>
#include <memory>
#include <string>

#include <QObject>

#include "AMonitorDisplay.hpp"

// Le programmeur, comme le poète, crée des abstractions proches de la pensée pure.
// Il crée des cathédrales dans les airs à partir de l'air lui-même, par le pouvoir de son imagination.
class AMonitorRenderer : public QObject {
	public:
		virtual ~AMonitorRenderer() = default;

		virtual void update() = 0;

		template<typename T, typename U, typename... Args>
		void addModule(Args &&...args) {
			addModule<T, U>(Qt::LeftDockWidgetArea, Qt::Vertical, std::forward<Args>(args)...);
		}

		template<typename T, typename U, typename... Args>
		void addModule(Qt::DockWidgetArea area, Qt::Orientation orientation, Args &&...args) {
			auto res = m_modules.emplace(typeid(U).hash_code(), new U(std::forward<Args>(args)...));
			res.first->second->setModuleInfo(new T);
			setupModule(typeid(U).hash_code(), area, orientation);
		}

		virtual void setupModule(size_t, Qt::DockWidgetArea, Qt::Orientation) {}

		template<typename T>
		IMonitorDisplay &getModule() {
			if (m_modules.find(typeid(T).hash_code()) == m_modules.end()) {
				throw std::exception();
			}

			return *dynamic_cast<T*>(m_modules.at(typeid(T).hash_code()));
		}

	protected:
		std::map<size_t, AMonitorDisplay *> m_modules;
};

#endif // AMONITORRENDERER_HPP_
