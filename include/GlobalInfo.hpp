#ifndef GLOBALINFO_HPP_
#define GLOBALINFO_HPP_

#include "AMonitorModule.hpp"

class GlobalInfo : public AMonitorModule {
	public:
	  GlobalInfo();
	  virtual ~GlobalInfo();

	  GlobalInfo &operator=(AMonitorModule const &);

	  virtual void FillData() override;

};

#endif //GLOBALINFO_HPP_
