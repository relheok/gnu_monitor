/*
 * =====================================================================================
 *
 *       Filename:  QSystemInfosDisplay.hpp
 *
 *    Description:
 *
 *        Created:  21/01/2017 23:42:46
 *
 *         Author:  Quentin Bazin, <quent42340@gmail.com>
 *
 * =====================================================================================
 */
#ifndef QSYSTEMINFOSDISPLAY_HPP_
#define QSYSTEMINFOSDISPLAY_HPP_

#include <QDockWidget>
#include <QLabel>

#include "AMonitorDisplay.hpp"

class QSystemInfosDisplay : public QDockWidget, public AMonitorDisplay {
	public:
		QSystemInfosDisplay(QWidget *parent = nullptr);

		void update();

	private:
		QLabel m_kernelVersionLabel;
		QLabel m_gccVersionLabel;
		QLabel m_operatingSystemLabel;
};

#endif // QSYSTEMINFOSDISPLAY_HPP_
