/*
 * =====================================================================================
 *
 *       Filename:  IMonitorDisplay.hpp
 *
 *    Description:
 *
 *        Created:  21/01/2017 19:59:28
 *
 *         Author:  Quentin Bazin, <quent42340@gmail.com>
 *
 * =====================================================================================
 */
#ifndef IMONITORDISPLAY_HPP_
#define IMONITORDISPLAY_HPP_

#include "IMonitorModule.hpp"

class IMonitorDisplay {
	public:
		virtual ~IMonitorDisplay() = default;

		virtual void update() = 0;

		virtual void setModuleInfo(IMonitorModule *info) = 0;
};

#endif // IMONITORDISPLAY_HPP_
