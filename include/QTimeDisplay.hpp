/*
 * =====================================================================================
 *
 *       Filename:  QTimeDisplay.hpp
 *
 *    Description:
 *
 *        Created:  22/01/2017 10:58:07
 *
 *         Author:  Quentin Bazin, <quent42340@gmail.com>
 *
 * =====================================================================================
 */
#ifndef QTIMEDISPLAY_HPP_
#define QTIMEDISPLAY_HPP_

#include <QDockWidget>

#include "AMonitorDisplay.hpp"
#include "QAnalogClock.hpp"
#include "QDigitalClock.hpp"

class QTimeDisplay : public QDockWidget, public AMonitorDisplay {
	Q_OBJECT

	public:
		QTimeDisplay(QWidget *parent = nullptr);

		void update();

	public slots:
		void changeClockDisplay(int state);

	private:
		QAnalogClock m_analogClock;
		QDigitalClock m_digitalClock;
};

#endif // QTIMEDISPLAY_HPP_
