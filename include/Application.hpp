/*
 * =====================================================================================
 *
 *       Filename:  Application.hpp
 *
 *    Description:
 *
 *        Created:  22/01/2017 04:02:07
 *
 *         Author:  Quentin Bazin, <quent42340@gmail.com>
 *
 * =====================================================================================
 */
#ifndef APPLICATION_HPP_
#define APPLICATION_HPP_

#include <memory>

#include <QApplication>
#include <QTimer>

#include "AMonitorRenderer.hpp"

class Application : public QApplication {
	public:
		Application(int argc, char **argv);

		int exec();

		AMonitorRenderer &monitorRenderer() const { return *m_monitorRenderer; }

	private:
		std::unique_ptr<AMonitorRenderer> m_monitorRenderer = nullptr;

		QTimer m_timer;
};

#endif // APPLICATION_HPP_
