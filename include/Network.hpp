#ifndef NETWORK_H
# define NETWORK_H

# include "AMonitorModule.hpp"

class Network : public AMonitorModule {
	public:
		Network();
		Network(AMonitorModule const &t);
		virtual ~Network();
		Network &operator=(AMonitorModule const &t);
		 std::string getAddress(std::string ip);
		void FillData(void) override;
};


#endif /* !NETWORK_H */
