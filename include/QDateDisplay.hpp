/*
 * =====================================================================================
 *
 *       Filename:  QDateDisplay.hpp
 *
 *    Description:
 *
 *        Created:  22/01/2017 10:56:52
 *
 *         Author:  Quentin Bazin, <quent42340@gmail.com>
 *
 * =====================================================================================
 */
#ifndef QDATEDISPLAY_HPP_
#define QDATEDISPLAY_HPP_

#include <QDockWidget>

#include "AMonitorDisplay.hpp"

class QDateDisplay : public QDockWidget, public AMonitorDisplay {
	public:
		QDateDisplay(QWidget *parent = nullptr);

		void update();
};

#endif // QDATEDISPLAY_HPP_
