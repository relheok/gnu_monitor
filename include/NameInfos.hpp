#ifndef NAMEINFOS_H
# define NAMEINFOS_H

# include <cstdio>
# include <unistd.h>
# include "AMonitorModule.hpp"

class			NameInfos : public AMonitorModule
{
public:
  NameInfos();
  NameInfos(NameInfos const &t);
  virtual ~NameInfos();
  NameInfos &operator=(AMonitorModule const &t);

  virtual void		FillData() override;

protected:
  std::map<std::string, std::string>	m_map;
};

#endif /* !NAMEINFOS_H */
