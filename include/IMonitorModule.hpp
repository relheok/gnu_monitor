#ifndef IMONITORMODULE_HPP_
#define IMONITORMODULE_HPP_

#include <algorithm>
#include <map>
#include <vector>
#include <fstream>
#include <iostream>

class IMonitorModule {
	public:
		virtual void FillData() = 0;

		virtual const std::map<std::string, std::string> &operator[](size_t) const = 0;
		virtual std::map<std::string, std::string> &operator[](size_t) = 0;

		virtual std::string getName() const = 0;
		virtual std::string getFile() const = 0;
		virtual unsigned int getMax() const = 0;

		virtual unsigned int	getSizeHistory() const = 0;

		virtual std::vector<std::map<std::string, std::string>> getData() const = 0;
};

#endif // IMONITORMODULE_HPP_
