#ifndef RAM_HPP_
#define RAM_HPP_

#include "AMonitorModule.hpp"

class RAM : public AMonitorModule {
	public:
		RAM();
		RAM(AMonitorModule const &);
		virtual ~RAM();

		RAM &operator=(AMonitorModule const &);

		virtual void FillData() override;
};

#endif // RAM_HPP_
