/*
 * =====================================================================================
 *
 *       Filename:  QNetworkDisplay.cpp
 *
 *    Description:
 *
 *        Created:  21/01/2017 22:29:19
 *
 *         Author:  Quentin Bazin, <quent42340@gmail.com>
 *
 * =====================================================================================
 */

#include <QChart>
#include <QChartView>
#include <QLineSeries>

#include "QNetworkDisplay.hpp"

QNetworkDisplay::QNetworkDisplay(QWidget *parent) : QDockWidget("Network load", parent) {
	QtCharts::QLineSeries *series = new QtCharts::QLineSeries();
	series->append(0, 6);
	series->append(2, 4);
	series->append(3, 8);
	series->append(7, 4);
	series->append(10, 5);
	*series << QPointF(11, 1) << QPointF(13, 3) << QPointF(17, 6) << QPointF(18, 3) << QPointF(20, 2);

	QtCharts::QChart *chart = new QtCharts::QChart();
	chart->legend()->hide();
	chart->addSeries(series);
	chart->createDefaultAxes();
	chart->axisX()->setLabelsVisible(false);
	chart->axisY()->setRange(0, 100);
	chart->setBackgroundVisible(false);
	// chart->setTitle("Simple line chart example");

	QtCharts::QChartView *chartView = new QtCharts::QChartView(chart);
	chartView->setRenderHint(QPainter::Antialiasing);

	setWidget(chartView);
}

void QNetworkDisplay::update() {
}

