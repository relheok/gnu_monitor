//
// NameInfos.cpp for  in piscineCPP/Rush/cpp_gkrellm/sources
//
// Made by Quentin Albertone
// Login   <albert_q@epitech.net>
//
// Started on  Sat Jan 21 11:05:02 2017 Quentin Albertone
// Last update Sun Jan 22 03:05:20 2017 Jérémy Koehler
//

#include "NameInfos.hpp"

NameInfos::NameInfos()
  : AMonitorModule("NameInfos", "/etc/hostname", 1)
{
}

NameInfos::NameInfos(NameInfos const &t)
  : AMonitorModule("NameInfos", "/etc/hostname", 1)
{
  if (this != &t)
    m_data = t.getData();
}

NameInfos&NameInfos::operator=(AMonitorModule const &t)
{
  if (this != &t)
    m_data = t.getData();
  return (*this);
}

NameInfos::~NameInfos()
{
}

void			NameInfos::FillData()
{
  std::ifstream			fd(m_filename, std::ifstream::in);
  std::string			line;
  std::string			login;

  if (!fd.is_open())
    {
      std::cerr << m_name << ": Error while open `" << m_filename << "`" << std::endl;
      return ;
    }

  if (fd.is_open() && fd.good())
    {
      std::getline(fd, line);
      m_map.insert(std::pair<std::string, std::string>("hostname", line));
      login = line.substr(3);
      m_map.insert(std::pair<std::string, std::string>("name", login));
    }
  m_data.push_back(m_map);
}
