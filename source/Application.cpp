/*
 * =====================================================================================
 *
 *       Filename:  Application.cpp
 *
 *    Description:
 *
 *        Created:  22/01/2017 04:07:43
 *
 *         Author:  Quentin Bazin, <quent42340@gmail.com>
 *
 * =====================================================================================
 */

#include "Application.hpp"
#include "CPU.hpp"
#include "GlobalInfo.hpp"
#include "NameInfos.hpp"
#include "Network.hpp"
#include "QCpuDisplay.hpp"
#include "QDateDisplay.hpp"
#include "QNetworkDisplay.hpp"
#include "QRamDisplay.hpp"
#include "QSystemInfosDisplay.hpp"
#include "QTimeDisplay.hpp"
#include "QUserInfosDisplay.hpp"
#include "QtMonitorRenderer.hpp"
#include "RAM.hpp"
#include "TermcapsCPU.hpp"
#include "TermcapsDate.hpp"
#include "TermcapsKernel.hpp"
#include "TermcapsNames.hpp"
#include "TermcapsNetwork.hpp"
#include "TermcapsRAM.hpp"
#include "TermcapsRenderer.hpp"

Application::Application(int argc, char **argv) : QApplication(argc, argv) {
	for (int i = 0 ; i < argc ; ++i) {
		if (std::string(argv[i]) == "-g") {
			auto *monitorRenderer = new QtMonitorRenderer();

			monitorRenderer->addModule<CPU, QCpuDisplay>(Qt::RightDockWidgetArea, Qt::Horizontal, monitorRenderer);
			monitorRenderer->addModule<RAM, QRamDisplay>(Qt::RightDockWidgetArea, Qt::Horizontal, monitorRenderer);
			monitorRenderer->addModule<Network, QNetworkDisplay>(Qt::RightDockWidgetArea, Qt::Horizontal, monitorRenderer);
			monitorRenderer->addModule<NameInfos, QUserInfosDisplay>(Qt::TopDockWidgetArea, Qt::Horizontal, monitorRenderer);
			monitorRenderer->addModule<GlobalInfo, QSystemInfosDisplay>(Qt::TopDockWidgetArea, Qt::Horizontal, monitorRenderer);
			monitorRenderer->addModule<RAM, QDateDisplay>(Qt::BottomDockWidgetArea, Qt::Horizontal, monitorRenderer);
			monitorRenderer->addModule<RAM, QTimeDisplay>(Qt::BottomDockWidgetArea, Qt::Horizontal, monitorRenderer);

			monitorRenderer->show();
			m_monitorRenderer.reset(monitorRenderer);

			break;
		}
	}

	if (!m_monitorRenderer) {
		m_monitorRenderer.reset(new Termcaps::Renderer());
		m_monitorRenderer->addModule<RAM, Termcaps::RAM>(3, 10);
		m_monitorRenderer->addModule<CPU, Termcaps::CPU>(43, 10);
		m_monitorRenderer->addModule<Network, Termcaps::Network>(3, 5);
		m_monitorRenderer->addModule<NameInfos, Termcaps::Names>(3, 2);
		m_monitorRenderer->addModule<CPU, Termcaps::Date>(43, 2);
		m_monitorRenderer->addModule<GlobalInfo, Termcaps::Kernel>(83, 2);
	}

	connect(&m_timer, &QTimer::timeout, m_monitorRenderer.get(), &AMonitorRenderer::update);

	m_timer.setInterval(100);
	m_timer.start();
}

int Application::exec() {
	return QApplication::exec();
}

