#include "CPU.hpp"

CPU::CPU()
  : AMonitorModule("CPU", "/proc/cpuinfo", 30)
{
}

CPU::CPU(AMonitorModule const &other)
  : AMonitorModule(other.getName(), other.getFile(), other.getMax())
{
	if (this != &other)
		if (!m_data.empty())
			m_data = other.getData();
}

CPU &CPU::operator=(AMonitorModule const &other)
{
	if (this != &other)
		if (!m_data.empty())
			m_data = other.getData();
	return (*this);
}

CPU::~CPU()
{
}

std::array<double, 9>	&CPU::getArray(int i)
{
	std::array<double, 9> *v = new std::array<double, 9>;
	std::string		line;
	std::ifstream		fs("/proc/stat", std::ifstream::in);

	if (!fs.is_open())
	  {
	    std::cerr << "CPU: Error while opening `/proc/stat`" << std::endl;
	    return (*v);
	  }
	while (i-- >= -1)
	  std::getline(fs, line);
       	line = line.substr(line.find(' ') + 1);
	i = -1;
	std::istringstream	is(line);
	// std::cout << "LINE " << line << std::endl;
	while (++i <= 8)
	  is >> (*v)[i];
       	fs.close();
	return (*v);
}

std::string	   CPU::UsageCPU(int i)
{
  std::string	   ret;
  std::array<double, 9> CPU;

  CPU = getArray(i);
  static double prev_total = 0;
  static double prev_idle = 0;
  double idle = CPU.at(3);
  double total = 0;
  double diff_idle = 0;
  double diff_total = 0;
  double diff_usage = 0;

  for (int j = 0; j < 9; j++)
    total = total + CPU[j];

  diff_idle = idle - prev_idle;
  diff_total = total - prev_total;
  diff_usage = (1000 * (diff_total - diff_idle) / diff_total + 5) / 10;
  ret = std::to_string(diff_usage);
  prev_total = total;
  prev_idle = idle;
  return (ret);
}

std::string	   CPU::haveField(std::string line, std::map<std::string, std::string> &map)
{
	std::stringstream	ss;
	static int		proc = 0;

	if (line.find("processor") != std::string::npos)
	  {
	    proc = std::atoi(line.substr(line.find(':') + 1).c_str());
	    map.emplace("usage", UsageCPU(-1));
	  }
	ss << line.substr(0, line.find(':')) << proc;
	return (ss.str());
}

void	CPU::FillData()
{
	std::map<std::string, std::string>	map;
	std::ifstream                 fd(m_filename, std::ifstream::in);
	std::string                   line;
	std::string                   field;
	std::string			cmp[5] = {"processor", "model name", "cpu MHz", "cache size", "cpu cores"};

	if (getSizeHistory() > m_max)
	  m_data.erase(m_data.begin());
	if (!fd.is_open())
	{
	  std::cerr << m_name << ": Error while opening `" << m_filename << "`" << std::endl;
	  return ;
	}
	while (std::getline(fd, line))
	  {
	    unsigned int i = 0;
	    while (i < 5)
	      {
		if (line.find(cmp[i]) == 0)
		  {
		    field = haveField(line, map);
		    field.erase(std::remove(field.begin(), field.end(), ' '), field.end());
		    field.erase(std::remove(field.begin(), field.end(), '\t'), field.end());
		    map.emplace(field, line.substr(line.find(":") + 1));
		  }
		++i;
	      }
	  }
	m_data.push_back(map);
	fd.close();
}
