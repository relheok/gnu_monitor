
#include "AMonitorModule.hpp"

AMonitorModule::AMonitorModule() : m_name(""), m_filename(""), m_max(0), m_data()
{
}

AMonitorModule::AMonitorModule(std::string const &name, std::string const &filename, unsigned int const max) : m_name(name), m_filename(filename), m_max(max), m_data()
{
}

AMonitorModule::~AMonitorModule()
{
  //delete m_data;
}

AMonitorModule::AMonitorModule(AMonitorModule const &other) : m_name(other.m_name), m_filename(other.m_filename), m_max(other.getMax())
{
  m_data = other.m_data;
}

AMonitorModule &AMonitorModule::operator=(AMonitorModule const &other)
{
  m_data = other.m_data;
  return (*this);
}

std::string	AMonitorModule::getName() const
{
  return (m_name);
}

std::string	AMonitorModule::getFile() const
{
  return (m_filename);
}

unsigned int	AMonitorModule::getMax() const
{
  return (m_max);
}

unsigned int		AMonitorModule::getSizeHistory() const
{
  return (m_data.size());
}

std::vector<std::map<std::string, std::string>> AMonitorModule::getData() const
{
  return (m_data);
}

const std::map<std::string, std::string> &AMonitorModule::operator[](size_t i) const
{
  return (m_data[i]);
}

std::map<std::string, std::string> &AMonitorModule::operator[](size_t i)
{
  return (m_data[i]);
}

