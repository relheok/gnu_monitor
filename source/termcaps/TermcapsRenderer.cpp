//
// termcaps.cpp for termcaps in /home/koehle_j/rendu/Piscine_tek2/cpp_gkrellm/termcapss
//
// Made by Jérémy Koehler
// Login   <koehle_j@epitech.net>
//
// Started on  Sat Jan 21 12:36:15 2017 Jérémy Koehler
// Last update Sun Jan 22 09:30:49 2017 Quentin Bazin
//

#include <ncurses.h>
#undef timeout

#include <QApplication>

#include "TermcapsRenderer.hpp"

Termcaps::Renderer::Renderer() {
  initscr();
  keypad(stdscr, TRUE);
  noecho();
  cbreak();
  nodelay(stdscr, 1);
  start_color();
  init_color(COLOR_BLUE, 305, 590, 844);
  init_pair(1, COLOR_WHITE, COLOR_BLACK);
  init_pair(2, COLOR_GREEN, COLOR_GREEN);
  init_pair(3, COLOR_YELLOW, COLOR_YELLOW);
  init_pair(4, COLOR_RED, COLOR_RED);
  init_pair(5, COLOR_CYAN, COLOR_BLACK);
  // init_pair(6, COLOR_BLUE, COLOR_BLUE);
  curs_set(FALSE);
}

Termcaps::Renderer::~Renderer() {
  endwin();
  for (auto &it : m_modules) {
    delete it.second;
  }
}

void Termcaps::Renderer::update() {
  int max_x;
  int max_y;

  getmaxyx(stdscr, max_y, max_x);
  if (getch() == 'q') {
	  qApp->exit();
  }
  if (max_y < 25 || max_x < 115) {
    for (int i = 0; i < max_y; ++i)
      for (int j = 0; j < max_x; ++j)
  	printw(" ");
    mvprintw(0, 0, "The screen is not large enough");
  }
  else {
    for (auto &it : m_modules) {
      mvprintw(0, 0, "                               ");
      it.second->update();
    }
  }
  refresh();
}

// bool Termcaps::Renderer::killRenderer() const {
//   char c = getch();

//   return (c == 27 || c == 'q');
// }
