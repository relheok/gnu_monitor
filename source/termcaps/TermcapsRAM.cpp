//
// TermcapsRAM.cpp for gkrellm in /home/koehle_j/rendu/Piscine_tek2/cpp_gkrellm/source/termcaps
//
// Made by Jérémy Koehler
// Login   <koehle_j@epitech.net>
//
// Started on  Sat Jan 21 22:43:11 2017 Jérémy Koehler
// Last update Sun Jan 22 09:30:49 2017 Quentin Bazin
//

#include <algorithm>
#include "TermcapsRAM.hpp"

Termcaps::RAM::RAM(int x, int y) {
  m_x = x;
  m_y = y;
  m_win = newwin(11, 32, y, x);
  box(m_win, 0, 0);
}

Termcaps::RAM::~RAM() {
  delwin(m_win);
}

void Termcaps::RAM::update() {
  m_info->FillData();
  std::string current(m_info->getData().at(0).at("current"));
  std::string max(m_info->getData().at(0).at("max"));
  wattron(m_win, A_BOLD);
  mvwprintw(m_win, 1, 1, "RAM:");
  wattroff(m_win, A_BOLD);
  mvwprintw(m_win, 1, 18, "[%c.%c%cG/%c.%c%cG]", current.at(0), current.at(1), current.at(2),
                                                 max.at(0), max.at(1), max.at(2));
  wattron(m_win, A_BOLD);
  displayGraphic(m_info->getData());
  wattroff(m_win, A_BOLD);
  wrefresh(m_win);
}

void Termcaps::RAM::setModuleInfo(IMonitorModule *info) {
  m_info.reset(info);
}

void Termcaps::RAM::displayGraphic(std::vector<std::map<std::string, std::string>> const &data) const {
  unsigned int i = 0;

  for (; i < 30 && i < data.size(); ++i) {
    long limit = ((double)stol(data.at(i).at("current")) / stol(data.at(i).at("max"))) * 8;

    for (int j = 0; j < 8; ++j) {
      if (j < limit) {
	wattron(m_win, COLOR_PAIR(2 + ((j + 1) * 2) / 7));
	mvwprintw(m_win, 9 - j, i + 1, " ");
      }
      else {
      	wattron(m_win, COLOR_PAIR(1) | A_BOLD);
      	mvwprintw(m_win, 9 - j, i + 1, ".");
      }
    }
  }
  for (; i < 30; ++i) {
    for (int j = 0; j < 8; ++j) {
      wattron(m_win, COLOR_PAIR(1) | A_BOLD);
      mvwprintw(m_win, 9 - j, i + 1, ".");
    }
  }
  attron(COLOR_PAIR(1));
}
