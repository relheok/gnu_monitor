//
// TermcapsCPU.cpp for gkrellm in /home/koehle_j/rendu/Piscine_tek2/cpp_gkrellm/source/termcaps
//
// Made by Jérémy Koehler
// Login   <koehle_j@epitech.net>
//
// Started on  Sat Jan 21 22:43:11 2017 Jérémy Koehler
// Last update Sun Jan 22 09:43:52 2017 Jérémy Koehler
//

#include <algorithm>
#include "TermcapsCPU.hpp"

Termcaps::CPU::CPU(int x, int y) {
  m_x = x;
  m_y = y;
  m_win = newwin(11, 32, y, x);
  box(m_win, 0, 0);
}

Termcaps::CPU::~CPU() {
  delwin(m_win);
}

void Termcaps::CPU::update() {
  m_info->FillData();
  wattron(m_win, A_BOLD);
  mvwprintw(m_win, 1, 1, "CPU:");
  displayGraphic(m_info->getData());
  wattroff(m_win, A_BOLD);
  wrefresh(m_win);
}

void Termcaps::CPU::setModuleInfo(IMonitorModule *info) {
  m_info.reset(info);
}

void Termcaps::CPU::displayGraphic(std::vector<std::map<std::string, std::string>> const &data) const {
  unsigned int i = 0;
  for (; i < 30 && i < data.size(); ++i) {
    int limit = (stof(data.at(i).at("usage")) / 100.0) * 8;

    for (int j = 0; j < 8; ++j) {
      if (j < limit) {
	wattron(m_win, COLOR_PAIR(2 + ((j + 1) * 2) / 7));
	mvwprintw(m_win, 9 - j, i + 1, " ");
      }
      else {
	wattron(m_win, COLOR_PAIR(1) | A_BOLD);
	mvwprintw(m_win, 9 - j, i + 1, ".");
      }
    }
  }
  for (; i < 30; ++i)
    for (int j = 0; j < 8; ++j)
      mvwprintw(m_win, 9 - j, i + 1, ".");
  wattron(m_win, COLOR_PAIR(1));
}
