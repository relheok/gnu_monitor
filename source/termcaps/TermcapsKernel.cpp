//
// TermcapsKernel.cpp for gkrellm in /home/koehle_j/rendu/Piscine_tek2/cpp_gkrellm/source/termcaps
//
// Made by Jérémy Koehler
// Login   <koehle_j@epitech.net>
//
// Started on  Sat Jan 21 22:43:11 2017 Jérémy Koehler
// Last update Sun Jan 22 09:30:49 2017 Quentin Bazin
//

#include <algorithm>
#include "TermcapsKernel.hpp"

Termcaps::Kernel::Kernel(int x, int y) {
  m_x = x;
  m_y = y;
  m_win = newwin(3, 32, y, x);
}

Termcaps::Kernel::~Kernel() {
  delwin(m_win);
}

void Termcaps::Kernel::update() {
  m_info->FillData();
  wattron(m_win, COLOR_PAIR(5));
  mvwprintw(m_win, 0, 0, "gcc version:");
  mvwprintw(m_win, 1, 0, "Kernel version:");
  mvwprintw(m_win, 2, 0, "System:");
  wattron(m_win, A_BOLD);
  mvwprintw(m_win, 0, 16, "%s", m_info->getData().at(0).at("gcc").c_str());
  mvwprintw(m_win, 1, 16, "%s", m_info->getData().at(0).at("version").c_str());
  mvwprintw(m_win, 2, 16, "%s", m_info->getData().at(0).at("system").c_str());
  wattroff(m_win, A_BOLD);
  wattron(m_win, COLOR_PAIR(1));
  wrefresh(m_win);
}

void Termcaps::Kernel::setModuleInfo(IMonitorModule *info) {
  m_info.reset(info);
}
