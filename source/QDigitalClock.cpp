/*
 * =====================================================================================
 *
 *       Filename:  QDigitalClock.cpp
 *
 *    Description:
 *
 *        Created:  22/01/2017 01:17:20
 *
 *         Author:  Quentin Bazin, <quent42340@gmail.com>
 *
 * =====================================================================================
 */

#include <QTime>
#include <QTimer>

#include "QDigitalClock.hpp"

QDigitalClock::QDigitalClock(QWidget *parent) : QLCDNumber(parent) {
	setDigitCount(8);
	setSegmentStyle(Filled);

	QTimer *timer = new QTimer(this);
	connect(timer, SIGNAL(timeout()), this, SLOT(showTime()));
	timer->start(1000);

	showTime();

	// resize(150, 60);
}

void QDigitalClock::showTime() {
	QTime time = QTime::currentTime();
	QString text = time.toString("hh:mm:ss");
	if ((time.second() % 2) == 0) {
		text[2] = ' ';
		text[5] = ' ';
	}
	display(text);
}

