
#include "RAM.hpp"

RAM::RAM() : AMonitorModule("RAM", "/proc/meminfo", 30)
{
}

RAM::RAM(AMonitorModule const &other) : AMonitorModule(other.getName(), other.getFile(), other.getMax())
{
}

RAM &RAM::operator=(AMonitorModule const &other)
{
	m_data = other.getData();
	return (*this);
}

RAM::~RAM()
{
}

void RAM::FillData()
{
	std::ifstream fd(m_filename, std::fstream::in);
	std::map<std::string, std::string> map;
	std::string line;
	std::string cmp[7] = {"MemTotal", "MemFree", "MemAvailable", "Buffers", "Cached", "Active", "Inactive"};
	unsigned int	i = 0;

	if (getSizeHistory() >= m_max)
	  m_data.erase(m_data.begin());
	if (!fd.is_open())
	{
		std::cerr << m_name << ": Error while opening `" << m_filename << "`" << std::endl;
		return ;
	}
	while (getline(fd, line) && i < 7)
	{
		line.erase(std::remove(line.begin(), line.end(), ' '), line.end());
		line.erase(std::remove(line.begin(), line.end(), '\t'), line.end());
		if (line.find(cmp[i]) == 0)
		{
		  map.insert(std::pair<std::string, std::string>
					(line.substr(0, line.find(":")),
					 line.substr(line.find(":") + 1, line.size())));
			++i;
		}
	}

	//	long a = atol(map["MemTotal"].c_str()) - atol(map["MemAvailable"].c_str()) - (atol(map["Active"].c_str()) - atol(map["Inactive"].c_str())) + (atol(map["Buffers"].c_str()) / 2);
	long a = atol(map["MemTotal"].c_str()) - atol(map["MemAvailable"].c_str());
	map.insert(std::pair<std::string, std::string>
		   ("max", map["MemTotal"]));
	map.insert(std::pair<std::string, std::string>
		   ("current", std::to_string(a)));
	m_data.push_back(map);
	fd.close();
}

